document.getElementById("btnNgayHomTruoc").onclick = function () {
  // input: tạo 3 biến ngay thang nam để lấy value từ nhập liệu của user
  var ngay = document.getElementById("ngay").value;
  var thang = document.getElementById("thang").value;
  var nam = document.getElementById("nam").value;
  // output: tạo biến kết quả tương ứng của ngày tháng năm của button ngày trước đó và ngày sau đó
  var ketQuaNgay = "";
  var ketQuaThang = "";
  var ketQuaNam = "";
  // Khối xử lý: xử dụng if else để lần lượt tính ra ngày, tháng, năm tiếp theo hoặc trước đó theo logic thông thường với các tháng có 28, 30, 31 và 29 ngày vào tháng 2 năm nhuận
  //   Tính ngày
  if (ngay == 1 && thang == 3 && nam % 4 == 0) {
    ketQuaNgay = 29;
  } else if (ngay == 1 && thang == 3) {
    ketQuaNgay = 28;
  } else if (ngay == 29 && thang == 2 && nam % 4 == 0) {
    ketQuaNgay = 28;
  } else if (ngay > 29 && thang == 2) {
    alert("Không hợp lê");
  } else if (
    (ngay == 1 && thang == 5) ||
    (ngay == 1 && thang == 7) ||
    (ngay == 1 && thang == 10) ||
    (ngay == 1 && thang == 12)
  ) {
    ketQuaNgay = 30;
  } else if (
    (ngay == 1 && thang == 1) ||
    (ngay == 1 && thang == 2) ||
    (ngay == 1 && thang == 4) ||
    (ngay == 1 && thang == 6) ||
    (ngay == 1 && thang == 8) ||
    (ngay == 1 && thang == 9) ||
    (ngay == 1 && thang == 11)
  ) {
    ketQuaNgay = 31;
  } else if (
    (ngay == 31 && thang == 4) ||
    (ngay == 31 && thang == 6) ||
    (ngay == 31 && thang == 9) ||
    (ngay == 31 && thang == 11)
  ) {
    alert("Không hợp lệ");
  } else if (ngay >= 32) {
    alert("Không hợp lệ");
  } else if (ngay <= 0) {
    alert("Không hợp lệ");
  } else if (ngay != 1) {
    ketQuaNgay = ngay - 1;
  }

  // Tính tháng
  if (thang == 1 && ngay == 1) {
    ketQuaThang = 12;
  } else if (thang > 12) {
    alert("Không hợp lệ");
  } else if (ngay == 1 && thang <= 12) {
    ketQuaThang = thang - 1;
  } else {
    ketQuaThang = thang;
  }
  //Tính năm
  if (ngay == 1 && thang == 1 && nam <= 0) {
    alert("Không hợp lệ");
  } else if (ngay == 1 && thang == 1) {
    ketQuaNam = nam - 1;
  } else {
    ketQuaNam = nam;
  }

  var ketQua = ketQuaNgay + "/" + ketQuaThang + "/" + ketQuaNam;
  document.getElementById("ketQua1").innerText = ketQua;
};
document.getElementById("btnNgayHomSau").onclick = function () {
  //Bước 1 đặt biến
  var ngay = document.getElementById("ngay").value * 1;
  var thang = document.getElementById("thang").value * 1;
  var nam = document.getElementById("nam").value * 1;

  var ketQuaNgay = "";
  var ketQuaThang = "";
  var ketQuaNam = "";
  //Tính ngày
  if (
    (ngay == 31 && thang == 1) ||
    (ngay == 31 && thang == 3) ||
    (ngay == 31 && thang == 5) ||
    (ngay == 31 && thang == 7) ||
    (ngay == 31 && thang == 8) ||
    (ngay == 31 && thang == 10) ||
    (ngay == 31 && thang == 12) ||
    (ngay == 30 && thang == 4) ||
    (ngay == 30 && thang == 6) ||
    (ngay == 30 && thang == 9) ||
    (ngay == 30 && thang == 11)
  ) {
    ketQuaNgay = 1;
  } else if (thang == 2 && ngay == 28 && nam % 4 == 0) {
    ketQuaNgay = 29;
  } else if (thang == 2 && ngay == 28) {
    ketQuaNgay = 1;
  } else if (thang == 2 && ngay == 29 && nam % 4 == 0) {
    ketQuaNgay = 1;
  } else if (thang == 2 && ngay >= 29) {
    alert("Không hợp lệ");
  } else if (ngay >= 32) {
    alert("Không hợp lệ");
  } else if (
    (ngay == 31 && thang == 4) ||
    (ngay == 31 && thang == 6) ||
    (ngay == 31 && thang == 9) ||
    (ngay == 31 && thang == 11)
  ) {
    alert("Không hợp lệ");
  } else {
    ketQuaNgay = ngay + 1;
  }
  //Tính tháng
  if (ngay == 31 && thang <= 11) {
    ketQuaThang = thang + 1;
  } else if (ngay == 31 && thang == 12) {
    ketQuaThang = 1;
  } else if (
    (ngay == 30 && thang == 4) ||
    (ngay == 30 && thang == 6) ||
    (ngay == 30 && thang == 9) ||
    (ngay == 30 && thang == 11)
  ) {
    ketQuaThang = thang + 1;
  } else if (thang == 2 && ngay == 29 && nam % 4 == 0) {
    ketQuaThang = 3;
  } else if (thang == 2 && ngay == 28 && nam % 4 == 0) {
    ketQuaThang = 2;
  } else if (thang == 2 && ngay == 28) {
    ketQuaThang = 3;
  } else {
    ketQuaThang = thang;
  }

  //Tính năm
  if (nam < 0) {
    alert("Không hợp lệ");
  } else if (ngay == 31 && thang == 12) {
    ketQuaNam = nam + 1;
  } else {
    ketQuaNam = nam;
  }

  var ketQua = ketQuaNgay + "/" + ketQuaThang + "/" + ketQuaNam;
  document.getElementById("ketQua1").innerText = ketQua;
};

/**
 * Bài 2
 */
document.getElementById("btnBai2").onclick = function () {
  // Input đặt biến để lấy dữ liệu từ người dùng nhập vào ô tháng và năm (number)
  var thang = document.getElementById("bai2Thang").value * 1;
  var nam = document.getElementById("bai2Nam").value * 1;
  // Output: đặt biến kết quả là số ngày mà tháng đó có (number)
  var ketQua2 = 0;
  // Progress: Đối với các tháng 1,3,5,7,8,10,12 sẽ có 31 ngày
  //Đối với các tháng 4,6,9,11 sẽ có 30 ngày
  //Đối với tháng 2, nếu là năm nhuận sẽ có 29 ngày. Ngược lại là 28 ngày
  // xử dụng if để làm
  if (thang > 12 || thang <= 0 || nam <= 0) {
    alert("Không hợp lệ");
  } else if (
    thang == 1 ||
    thang == 3 ||
    thang == 5 ||
    thang == 7 ||
    thang == 8 ||
    thang == 10 ||
    thang == 12
  ) {
    ketQua2 = 31;
  } else if (thang == 4 || thang == 6 || thang == 9 || thang == 11) {
    ketQua2 = 30;
  } else if (thang == 2 && nam % 4 == 0) {
    ketQua2 = 29;
  } else if (thang == 2) {
    ketQua2 = 28;
  }
  document.getElementById(
    "ketQua2"
  ).innerText = `Tháng ${thang} năm ${nam} có ${ketQua2} ngày`;
};
/**
 * Bài 3
 */
document.getElementById("btnBai3").onclick = function () {
  //input: tạo biến chưa giá trị được user nhập vào
  var soNguyen = document.getElementById("nhapSoNguyen3").value * 1;
  //output: string : cách đọc số nguyên đó tương ứng với hàng trăm hàng chục và hàng đơn vị
  var hangTram = "";
  var hangChuc = "";
  var hangDonVi = "";
  var soAm = "";

  //progress đặt điều kiện để số đó phải là số nguyên có 3 chữ số :
  // sau đó chia ra hai trường hợp là số nguyên có 3 chữ số âm hay dương
  // nếu là số dương phải >=100 hoặc <1000
  //nếu là số âm thì phải <=-100 hoặc >-1000
  // sau đó sẽ tính theo công thức để tìm từng số hàng trăm, hàng chục, hàng đơn vị, đối với số dương ta dùng hàm Math.floor để làm tròn số xuống, đối với số âm ta dùng hàm Math.ceil để làm tròn số lên, các case của trường hợp số âm ta thêm dấu - vào
  if (soNguyen >= 100 && soNguyen < 1000) {
    soAm = "";
    var soHangTram = Math.floor(soNguyen / 100);
    var soHangChuc = Math.floor((soNguyen % 100) / 10);
    var soHangDonVi = soNguyen % 10;
    //Đọc số hàng trăm
    switch (soHangTram) {
      case 1:
        {
          hangTram = "Một trăm";
        }
        break;
      case 2:
        {
          hangTram = "Hai trăm";
        }
        break;
      case 3:
        {
          hangTram = "Ba trăm";
        }
        break;
      case 4:
        {
          hangTram = "Bốn trăm";
        }
        break;
      case 5:
        {
          hangTram = "Năm trăm";
        }
        break;
      case 6:
        {
          hangTram = "Sáu trăm";
        }
        break;
      case 7:
        {
          hangTram = "Bảy trăm";
        }
        break;
      case 8:
        {
          hangTram = "Tám trăm";
        }
        break;
      case 9:
        {
          hangTram = "Chín trăm";
        }
        break;
    }
    //Đọc số hàng chục
    switch (soHangChuc) {
      case 0:
        {
          hangChuc = "lẻ";
        }
        break;
      case 1:
        {
          hangChuc = "mười";
        }
        break;
      case 2:
        {
          hangChuc = "hai mươi";
        }
        break;
      case 3:
        {
          hangChuc = "ba mươi";
        }
        break;
      case 4:
        {
          hangChuc = "bốn mươi";
        }
        break;
      case 5:
        {
          hangChuc = "năm mươi";
        }
        break;
      case 6:
        {
          hangChuc = "sáu mươi";
        }
        break;
      case 7:
        {
          hangChuc = "bảy mươi";
        }
        break;
      case 8:
        {
          hangChuc = "tám mươi";
        }
        break;
      case 9:
        {
          hangChuc = "chín mươi";
        }
        break;
    }
    //Đọc số hàng đơn vị
    switch (soHangDonVi) {
      case 0:
        {
          hangDonVi = "";
        }
        break;
      case 1:
        {
          hangDonVi = "mốt";
        }
        break;
      case 2:
        {
          hangDonVi = "hai";
        }
        break;
      case 3:
        {
          hangDonVi = "ba";
        }
        break;
      case 4:
        {
          hangDonVi = "bốn";
        }
        break;
      case 5:
        {
          hangDonVi = "lăm";
        }
        break;
      case 6:
        {
          hangDonVi = "sáu";
        }
        break;
      case 7:
        {
          hangDonVi = "bảy";
        }
        break;
      case 8:
        {
          hangDonVi = "tám";
        }
        break;
      case 9:
        {
          hangDonVi = "chín";
        }
        break;
    }
    if (soHangChuc == 0 && soHangDonVi == 0) {
      hangChuc = "";
    }
    if (soHangChuc == 0 && soHangDonVi == 1) {
      hangDonVi = "một";
    }
    if (soHangChuc == 0 && soHangDonVi == 5) {
      hangDonVi = "năm";
    }
    if (soHangChuc == 1 && soHangDonVi == 1) {
      hangDonVi = "một";
    }
  } else if (soNguyen <= -100 && soNguyen > -1000) {
    soAm = "âm";
    var soHangTram = Math.ceil(soNguyen / 100);
    var soHangChuc = Math.ceil((soNguyen % 100) / 10);
    var soHangDonVi = soNguyen % 10;
    //Đọc số hàng trăm
    switch (soHangTram) {
      case -1:
        {
          hangTram = "Một trăm";
        }
        break;
      case -2:
        {
          hangTram = "Hai trăm";
        }
        break;
      case -3:
        {
          hangTram = "Ba trăm";
        }
        break;
      case -4:
        {
          hangTram = "Bốn trăm";
        }
        break;
      case -5:
        {
          hangTram = "Năm trăm";
        }
        break;
      case -6:
        {
          hangTram = "Sáu trăm";
        }
        break;
      case -7:
        {
          hangTram = "Bảy trăm";
        }
        break;
      case -8:
        {
          hangTram = "Tám trăm";
        }
        break;
      case -9:
        {
          hangTram = "Chín trăm";
        }
        break;
    }
    //Đọc số hàng chục
    switch (soHangChuc) {
      case 0:
        {
          hangChuc = "lẻ";
        }
        break;
      case -1:
        {
          hangChuc = "mười";
        }
        break;
      case -2:
        {
          hangChuc = "hai mươi";
        }
        break;
      case -3:
        {
          hangChuc = "ba mươi";
        }
        break;
      case -4:
        {
          hangChuc = "bốn mươi";
        }
        break;
      case -5:
        {
          hangChuc = "năm mươi";
        }
        break;
      case -6:
        {
          hangChuc = "sáu mươi";
        }
        break;
      case -7:
        {
          hangChuc = "bảy mươi";
        }
        break;
      case -8:
        {
          hangChuc = "tám mươi";
        }
        break;
      case -9:
        {
          hangChuc = "chín mươi";
        }
        break;
    }
    //Đọc số hàng đơn vị
    switch (soHangDonVi) {
      case 0:
        {
          hangDonVi = "";
        }
        break;
      case -1:
        {
          hangDonVi = "mốt";
        }
        break;
      case -2:
        {
          hangDonVi = "hai";
        }
        break;
      case -3:
        {
          hangDonVi = "ba";
        }
        break;
      case -4:
        {
          hangDonVi = "bốn";
        }
        break;
      case -5:
        {
          hangDonVi = "lăm";
        }
        break;
      case -6:
        {
          hangDonVi = "sáu";
        }
        break;
      case -7:
        {
          hangDonVi = "bảy";
        }
        break;
      case -8:
        {
          hangDonVi = "tám";
        }
        break;
      case -9:
        {
          hangDonVi = "chín";
        }
        break;
    }
    if (soHangChuc == 0 && soHangDonVi == 0) {
      hangChuc = "";
    }
    if (soHangChuc == 0 && soHangDonVi == -1) {
      hangDonVi = "một";
    }
    if (soHangChuc == 0 && soHangDonVi == -5) {
      hangDonVi = "năm";
    }
    if (soHangChuc == -1 && soHangDonVi == -1) {
      hangDonVi = "một";
    }
  } else {
    alert("Vui lòng nhập số có 3 chữ số");
  }

  var ketQua3 = soAm + " " + hangTram + " " + hangChuc + " " + hangDonVi;
  document.getElementById("ketQua3").innerText = ketQua3;
};
/**
 * Bài 4
 */
document.getElementById("btnBai4").onclick = function () {
  //input: number đối với tọa độ x,y của sinh viên
  var x1 = document.getElementById("x1").value * 1;
  var x2 = document.getElementById("x2").value * 1;
  var x3 = document.getElementById("x3").value * 1;
  var y1 = document.getElementById("y1").value * 1;
  var y2 = document.getElementById("y2").value * 1;
  var y3 = document.getElementById("y3").value * 1;
  var xt = document.getElementById("xt").value * 1;
  var yt = document.getElementById("yt").value * 1;
  //output: string đối với tên sinh viên
  var sv1 = document.getElementById("sv1").value;
  var sv2 = document.getElementById("sv2").value;
  var sv3 = document.getElementById("sv3").value;
  //progress: tính đoạn đường từ nhà sinh viên đến trường theo công thức tính độ dài đoạn thẳng theo tọa độ. Sau đó ta so sánh các đoạn thẳng với nhau xem đoạn nào xa nhất tương ứng sẽ là nhà sinh viên đó xa nhất
  var d1 = Math.sqrt((x1 - xt) * (x1 - xt) + (y1 - yt) * (y1 - yt));
  var d2 = Math.sqrt((x2 - xt) * (x2 - xt) + (y2 - yt) * (y2 - yt));
  var d3 = Math.sqrt((x3 - xt) * (x3 - xt) + (y3 - yt) * (y3 - yt));
  var max = d1;
  if (max < d2) {
    max = d2;
  }
  if (max < d3) {
    max = d3;
  }
  var ketQua4 = "";
  if (max == d1) {
    ketQua4 = ` Nhà sinh viên xa nhà nhất là ${sv1}`;
  } else if (max == d2) {
    ketQua4 = ` Nhà sinh viên xa nhà nhất là ${sv2}`;
  } else if (max == d3) {
    ketQua4 = ` Nhà sinh viên xa nhà nhất là ${sv3}`;
  }
  document.getElementById("ketQua4").innerText = ketQua4;
};

/**
 * Bài 1
 */
// Bước 1 : tạo function click vào button
// Sau đó tạo biến input là số người dùng nhập vào
document.getElementById("btnBai1").onclick = function () {
  var nhapSo1 = document.getElementById("bai1So1").value * 1;
  var nhapSo2 = document.getElementById("bai1So2").value * 1;
  var nhapSo3 = document.getElementById("bai1So3").value * 1;
  //Bước 2 tạo output là kết quả xuất ra màn hình là thứ tự sắp xếp của 3 biến trên => string

  var ketQua = "";

  //Bước 3 xử lý: thiết lập ra các điều kiện ứng với các điều kiện trong if ( số thứ 1 nhỏ nhât, số thứ 2 nhỏ nhất, số thứ 3 nhỏ nhất... trong đó số thứ 1 nhỏ nhất sẽ có 2 trường hợp xảy ra là 1 2 3 và 1 3 2, tương tự với 2 số còn lại)
  if (nhapSo1 < nhapSo2 && nhapSo1 < nhapSo3 && nhapSo2 < nhapSo3) {
    ketQua = nhapSo1 + "," + nhapSo2 + "," + nhapSo3;
  } else if (nhapSo1 < nhapSo2 && nhapSo1 < nhapSo3 && nhapSo3 < nhapSo2) {
    ketQua = nhapSo1 + "," + nhapSo3 + "," + nhapSo2;
  } else if (nhapSo2 < nhapSo1 && nhapSo2 < nhapSo3 && nhapSo1 < nhapSo3) {
    ketQua = nhapSo2 + "," + nhapSo1 + "," + nhapSo3;
  } else if (nhapSo2 < nhapSo1 && nhapSo2 < nhapSo3 && nhapSo3 < nhapSo1) {
    ketQua = nhapSo2 + "," + nhapSo3 + "," + nhapSo1;
  } else if (nhapSo3 < nhapSo1 && nhapSo3 < nhapSo2 && nhapSo1 < nhapSo2) {
    ketQua = nhapSo3 + "," + nhapSo1 + "," + nhapSo2;
  } else if (nhapSo3 < nhapSo1 && nhapSo3 < nhapSo2 && nhapSo2 < nhapSo1) {
    ketQua = nhapSo3 + "," + nhapSo2 + "," + nhapSo1;
  }
  document.getElementById("ketQua1").innerText = ketQua;
};

/**
 * Bài 2 */
document.getElementById("btnBai2").onclick = function () {
  //bước 1 xác định input là value mà user chọn, tạo biến rồi DOM vào giá trị của dropdown đã tạo bên thẻ html
  var chonThanhVien = document.getElementById("chonThanhVien").value;
  //Bước 2 xác định input là lời chào là 1 chuỗi string
  var loiChao = "";
  //bước 3 xử lý: dùng hàm if, tương ứng với mỗi giá trị user chọn sẽ xuất ra 1 lời chao tương ứng, sau đó gán kết quả vào biến loiChao
  if (chonThanhVien == "B") {
    loiChao = "Xin chào Bố!";
  } else if (chonThanhVien == "M") {
    loiChao = "Xin chào Mẹ!";
  } else if (chonThanhVien == "A") {
    loiChao = "Xin chào Anh trai!";
  } else if (chonThanhVien == "E") {
    loiChao = "Xin chào Em gái!";
  } else {
    loiChao = "Máy không dành cho người lạ";
  }
  document.getElementById("ketQua2").innerText = loiChao;
};
/**
 * Bài 3
 */
document.getElementById("btnBai3").onclick = function () {
  //Bước 1: xác định input là number, là giá trị mà user nhập vào, tạo 3 biến tương ứng với 3 giá trị, ép kiểu dữ liệu là number
  var soNguyen1 = document.getElementById("bai3So1").value;
  var soNguyen2 = document.getElementById("bai3So2").value;
  var soNguyen3 = document.getElementById("bai3So3").value;
  //Bước 2 xác định output là number , tạo biến đếm để đếm số lượng số chẵn và số lẻ
  var demSoChan = 0;

  //Bước 3 xử lý, lần lượt chia các số cho 2 dùng phép chia lấy dư, nếu số nào là số chẵn thì +1 giá trị vào biến demSoChan, sau cùng lấy 3 - demSoChan sẽ ra đc số lẻ

  if (soNguyen1 % 2 == 0) {
    demSoChan++;
  }
  if (soNguyen2 % 2 == 0) {
    demSoChan++;
  }
  if (soNguyen3 % 2 == 0) {
    demSoChan++;
  }
  var demSoLe = 3 - demSoChan;

  if (soNguyen1 == "" || soNguyen2 == "" || soNguyen3 == "") {
    alert("Dữ liệu không hợp lệ, vui lòng điền đầy đủ 3 số vào các ô trống");
    demSoChan = "";
    demSoLe = "";
  }

  document.getElementById("ketQua31").innerText = demSoChan;
  document.getElementById("ketQua32").innerText = demSoLe;
};

/**
 * Bài 4
 */
document.getElementById("btnBai4").onclick = function () {
  //Bước 1 xác định input là number, tạo biến chứa giá trị user nhập vào
  var canh1 = document.getElementById("doDaiCanh1").value * 1;
  var canh2 = document.getElementById("doDaiCanh2").value * 1;
  var canh3 = document.getElementById("doDaiCanh3").value * 1;
  //Bước 2 xác định output là string, kết quả sẽ xuất ra
  var ketQua4 = "";
  //Bước 3 xử lý
  if (canh1 + canh2 > canh3 && canh1 + canh3 > canh2 && canh2 + canh3 > canh1) {
    if (
      (canh1 == canh2 && canh1 != canh3) ||
      (canh1 == canh3 && canh1 != canh2) ||
      (canh2 == canh3 && canh2 != canh1)
    ) {
      ketQua4 = "Đây là tam giác cân";
    } else if (canh1 == canh2 && canh1 == canh3) {
      ketQua4 = "Đây là tam giác đều";
    } else if (
      canh1 * canh1 == canh2 * canh2 + canh3 * canh3 ||
      canh2 * canh2 == canh1 * canh1 + canh3 * canh3 ||
      canh3 * canh3 == canh2 * canh2 + canh1 * canh1
    ) {
      ketQua4 = "Đây là tam giác vuông";
    } else {
      ketQua4 = "Đây là tam giác bình thường";
    }
  } else {
    ketQua4 = "Đây không phải là hình tam giác";
  }
  document.getElementById("ketQua4").innerText = ketQua4;
};
